CREATE DATABASE  IF NOT EXISTS `fast_vps` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `fast_vps`;
-- MySQL dump 10.13  Distrib 5.5.41, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: fast_vps
-- ------------------------------------------------------
-- Server version	5.5.41-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `valuta`
--

DROP TABLE IF EXISTS `valuta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `valuta` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `valute_attr_id` varchar(7) NOT NULL,
  `num_code` char(3) NOT NULL,
  `char_code` char(3) NOT NULL,
  `name_ru` varchar(255) NOT NULL,
  `name_en` varchar(255) DEFAULT NULL,
  `default_active` enum('Y','N') NOT NULL,
  `status` enum('Y','N') NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `valuta`
--

LOCK TABLES `valuta` WRITE;
/*!40000 ALTER TABLE `valuta` DISABLE KEYS */;
INSERT INTO `valuta` VALUES (1,'R01010','036','AUD','Австралийский доллар',NULL,'N','Y'),(2,'R01020A','944','AZN','Азербайджанский манат',NULL,'N','Y'),(3,'R01035','826','GBP','Фунт стерлингов Соединенного королевства',NULL,'N','Y'),(4,'R01060','051','AMD','Армянских драмов',NULL,'N','Y'),(5,'R01090','974','BYR','Белорусских рублей',NULL,'Y','Y'),(6,'R01100','975','BGN','Болгарский лев',NULL,'N','Y'),(7,'R01115','986','BRL','Бразильский реал',NULL,'N','Y'),(8,'R01135','348','HUF','Венгерских форинтов',NULL,'N','Y'),(9,'R01215','208','DKK','Датских крон',NULL,'N','Y'),(10,'R01235','840','USD','Доллар США',NULL,'Y','Y'),(11,'R01239','978','EUR','Евро',NULL,'Y','Y'),(12,'R01270','356','INR','Индийских рупий',NULL,'N','Y'),(13,'R01335','398','KZT','Казахских тенге',NULL,'N','Y'),(14,'R01350','124','CAD','Канадский доллар',NULL,'N','Y'),(15,'R01370','417','KGS','Киргизских сомов',NULL,'N','Y'),(16,'R01375','156','CNY','Китайских юаней',NULL,'N','Y'),(17,'R01500','498','MDL','Молдавских леев',NULL,'N','Y'),(18,'R01535','578','NOK','Норвежских крон',NULL,'N','Y'),(19,'R01565','985','PLN','Польский злотый',NULL,'N','Y'),(20,'R01585F','946','RON','Новый румынский лей',NULL,'N','Y'),(21,'R01589','960','XDR','СДР (специальные права заимствования)',NULL,'N','Y'),(22,'R01625','702','SGD','Сингапурский доллар',NULL,'N','Y'),(23,'R01670','972','TJS','Таджикский сомони',NULL,'N','Y'),(24,'R01700J','949','TRY','Турецкая лира',NULL,'N','Y'),(25,'R01710A','934','TMT','Новый туркменский манат',NULL,'N','Y'),(26,'R01717','860','UZS','Узбекских сумов',NULL,'N','Y'),(27,'R01720','980','UAH','Украинских гривен',NULL,'Y','Y'),(28,'R01760','203','CZK','Чешских крон',NULL,'N','Y'),(29,'R01770','752','SEK','Шведских крон',NULL,'N','Y'),(30,'R01775','756','CHF','Швейцарский франк',NULL,'N','Y'),(31,'R01810','710','ZAR','Южноафриканских рэндов',NULL,'N','Y'),(32,'R01815','410','KRW','Вон Республики Корея',NULL,'N','Y'),(33,'R01820','392','JPY','Японских иен',NULL,'N','Y');
/*!40000 ALTER TABLE `valuta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `valuta_update`
--

DROP TABLE IF EXISTS `valuta_update`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `valuta_update` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `valuta_update`
--

LOCK TABLES `valuta_update` WRITE;
/*!40000 ALTER TABLE `valuta_update` DISABLE KEYS */;
INSERT INTO `valuta_update` VALUES (1,'2015-02-20');
/*!40000 ALTER TABLE `valuta_update` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `valuta_value`
--

DROP TABLE IF EXISTS `valuta_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `valuta_value` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `valuta_id` smallint(5) unsigned NOT NULL,
  `valuta_value` varchar(10) NOT NULL,
  `valuta_nominal` mediumint(8) unsigned NOT NULL,
  `date_update` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `valuta_id` (`valuta_id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `valuta_value`
--

LOCK TABLES `valuta_value` WRITE;
/*!40000 ALTER TABLE `valuta_value` DISABLE KEYS */;
INSERT INTO `valuta_value` VALUES (1,1,'48,3377',1,'2015-02-20'),(2,2,'79,1272',1,'2015-02-20'),(3,3,'95,9733',1,'2015-02-20'),(4,4,'12,9709',100,'2015-02-20'),(5,5,'41,2554',10000,'2015-02-20'),(6,6,'36,3422',1,'2015-02-20'),(7,7,'21,8924',1,'2015-02-20'),(8,8,'23,2308',100,'2015-02-20'),(9,9,'95,4829',10,'2015-02-20'),(10,10,'62,1307',1,'2015-02-20'),(11,11,'70,9408',1,'2015-02-20'),(12,12,'10,0077',10,'2015-02-20'),(13,13,'33,5642',100,'2015-02-20'),(14,14,'49,8401',1,'2015-02-20'),(15,15,'10,1661',10,'2015-02-20'),(16,16,'99,3281',10,'2015-02-20'),(17,17,'30,9493',10,'2015-02-20'),(18,18,'82,5855',10,'2015-02-20'),(19,19,'16,9909',1,'2015-02-20'),(20,20,'15,9949',1,'2015-02-20'),(21,21,'87,8267',1,'2015-02-20'),(22,22,'45,7617',1,'2015-02-20'),(23,23,'11,2180',1,'2015-02-20'),(24,24,'25,5000',1,'2015-02-20'),(25,25,'17,7542',1,'2015-02-20'),(26,26,'25,2564',1000,'2015-02-20'),(27,27,'22,8422',10,'2015-02-20'),(28,28,'25,9386',10,'2015-02-20'),(29,29,'74,4660',10,'2015-02-20'),(30,30,'66,0333',1,'2015-02-20'),(31,31,'53,7356',10,'2015-02-20'),(32,32,'56,0428',1000,'2015-02-20'),(33,33,'52,3823',100,'2015-02-20');
/*!40000 ALTER TABLE `valuta_value` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-03-02  5:56:15
