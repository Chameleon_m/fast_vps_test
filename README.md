# FAST VPS TEST

> ## Requirements.
> 
> 1.   PHP 5.4 and up.
> 2.   Phalcon >= 1.3.0. (recommended latest version)
> 3.   For branch "beanstalk" need to install [ Beanstalkd ](http://kr.github.io/beanstalkd/)
>

### PHALCON
[ Installation ](http://docs.phalconphp.com/en/latest/reference/install.html)

### Using Git


### NGINX CONFIGURATION

~~~~
server {

    listen   80;
    server_name fastvps.local;
    root /var/www/fast_vps_test/public;
    
    index index.php;

    #access_log /var/log/nginx/fastvps-acc.log;
    #error_log /var/log/nginx/fastvps-err.log; 

    location = /robots.txt { return 204; access_log off; log_not_found off; }
    location = /favicon.ico { return 204; access_log off; log_not_found off; }
    #location ~ /\. { access_log off; log_not_found off; allow all; }
    #location ~ ~$ { access_log off; log_not_found off; deny all; }

    location / {
        try_files $uri $uri/ /index.php;
    }

    location /css/ {
        expires -1;
    }

    location /img/ {
        expires -1;
    }

    location /js/ {
        expires -1;
    }

    location ~ \.php$ {
        fastcgi_pass unix:/var/run/php5-fpm.sock;
        fastcgi_index  index.php;
        fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
        include        fastcgi_params;
        fastcgi_param   APPLICATION_ENV production;
    }

}
~~~~

### Lib install
Composer install
~~~~
curl -s https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer
~~~~
in site dir
~~~~
composer update
~~~~

### Create base

sql path - /schema/Dump.sql

### Grant Permissions
~~~~
sudo chown -R www-data:www-data /var/www/fast_vps_test
sudo chmod 755 /var/www
~~~~

### Update valuts

~~~~
php /var/www/fast_vps_test/private/index.php currency checkValuta
~~~~
or use cron 
~~~~
0 * * * * php /var/www/fast_vps_test/private/index.php currency checkValuta
~~~~