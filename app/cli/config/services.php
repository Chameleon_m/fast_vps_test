<?php

$di->set('router', function() {
    $router = new \Phalcon\CLI\Router();
    return $router;
});

//Registering a dispatcher
$di->set('dispatcher', function() {
    //Create/Get an EventManager
    $eventsManager = new \Phalcon\Events\Manager();
    //Attach a listener
    $eventsManager->attach("dispatch", function($event, $dispatcher, $exception) {
        //controller or action doesn't exist
        if ($event->getType() == 'beforeException') {
            switch ($exception->getCode()) {
                case \Phalcon\Cli\Dispatcher::EXCEPTION_HANDLER_NOT_FOUND:
                case \Phalcon\Dispatcher::EXCEPTION_ACTION_NOT_FOUND:
                    $dispatcher->forward(array(
                        'task' => 'main',
                        'action' => 'notFound'
                    ));
                    return false;
            }
        }
    });
    
    $dispatcher = new \Phalcon\Cli\Dispatcher();
    //Set default namespace to frontend module
    $dispatcher->setDefaultNamespace("FastVps\Cli\Tasks");
    //Bind the EventsManager to the dispatcher
    $dispatcher->setEventsManager($eventsManager);

    return $dispatcher;
});

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->set('db', function() use ($di) {
    
    if ($di->get('config')->database_log) {
        
        $connection = new \Phalcon\Db\Adapter\Pdo\Mysql($di->get('config')->database->toArray());
        $eventsManager = new \Phalcon\Events\Manager();
        // Создание слушателя базы данных
        $dbListener = new \FastVps\Common\Plugins\DbLog($di->get('config')->database_log_path);
        // Слушать все события базы данных
        $eventsManager->attach('db', $dbListener);
        // Совмещение менеджера событий с адаптером базы данных
        $connection->setEventsManager($eventsManager);
        
    } else {
        $connection = new \Phalcon\Db\Adapter\Pdo\Mysql($di->get('config')->database->toArray());
    }
    
    return $connection;
});