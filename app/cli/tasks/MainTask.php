<?php

namespace FastVps\Cli\Tasks;

use Phalcon\CLI\Task;

/**
 * Main CLI Task
 */
class MainTask extends Task
{

    /**
     * Initialize
     */
    public function initialize()
    {
        
    }

    /**
     * Main Action
     */
    public function mainAction()
    {
        echo "-- CLI tasks --\n";
        /**
         * Выводит список задач и экшенов
         */
        foreach (new \DirectoryIterator(__DIR__) as $file) {
            if ($file->isDot() || $file->getBasename('.php') == 'MainTask') {
                continue;
            }
            $task = $file->getBasename('.php');

            $f = new \ReflectionClass(__NAMESPACE__ . '\\' . $task);
            $doc_task = $f->getDocComment();
            if (empty($doc_task)) {
                echo "# no DocBlock for the Task" . PHP_EOL;
            } else {
                echo $doc_task . PHP_EOL;
            }

            echo strtolower(strstr($task, 'Task', TRUE)) . PHP_EOL;
            foreach ($f->getMethods() as $m) {

                if ($m->class == __NAMESPACE__ . '\\' . $task && substr($m->name, -6) === 'Action') {
                    $doc_action = $m->getDocComment();
                    if (empty($doc_action)) {
                        echo "    # no DocBlock for the Action" . PHP_EOL;
                    } else {
                        echo "    " . $doc_action . PHP_EOL;
                    }
                    echo "--> " . strstr($m->name, 'Action', TRUE) . PHP_EOL . PHP_EOL;
                }
            }
        }
    }

    /**
     * Not found Action
     */
    public function notFoundAction()
    {
        echo "Task not found\n";
    }

}
