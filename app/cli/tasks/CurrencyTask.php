<?php

namespace FastVps\Cli\Tasks;

use FastVps\Cli\Tasks\MainTask;
use FastVps\Common\Models\Valuta;
use FastVps\Common\Models\ValutaValue;
use FastVps\Common\Models\ValutaUpdate;

/**
 * Currency CLI Task
 * @tutorial php index.php taskname actionname param param ...
 */
class CurrencyTask extends MainTask
{
    use \FastVps\Common\Plugins\Traits\View\ClearCache;

    public function mainAction()
    {
        echo "CurrencyTask/mainAction\n";
    }

    /**
     * Обновляет курсы валют
     * @example php index.php currency checkValuta
     */
    public function checkValutaAction()
    {
        echo "Start 'Check valuta'\n";

        $reader = new \XMLReader();
        $reader->open('http://www.cbr.ru/scripts/XML_daily.asp');
        $reader->read();
        if ($reader->name == "ValCurs") {
            // Дата внесения изменения  в cbr.
            $date = $reader->getAttribute('Date');
            $date = (new \DateTime($date))->format('Y-m-d');
            // Дата последнего обновленя данных
            $last_update = ValutaUpdate::findFirst();
            if ($last_update != NULL) {
                $last_update_date = (new \DateTime($last_update->date))->format('Y-m-d');
                if ($date == $last_update_date) {
                    die("Данные не обновлялись" . PHP_EOL);
                }
            } else {
                $last_update = new ValutaUpdate();
            }
        } else {
            // Уведомляем
            throw new \Exception("Изменился формат данных." . PHP_EOL);
        }

        while ($reader->read()) {

            $item = [];

            if ($reader->nodeType == \XMLReader::ELEMENT && $reader->name == "Valute") {

                if ($reader->hasAttributes) {
                    $item['id'] = $reader->getAttribute('ID');
                }

                while ($reader->read()) {

                    switch ($reader->nodeType) {
                        case \XMLReader::SIGNIFICANT_WHITESPACE:
                            continue;
                            break;
                        case \XMLReader::ELEMENT:
                            $element_name = $reader->name;
                            break;
                        case \XMLReader::TEXT:
                            $item[$element_name] = $reader->value;
                            break;
                        case \XMLReader::END_ELEMENT:
                            if ($reader->name == "Valute") {
                                break(2);
                            } else {
                                break;
                            }
                        default:
                            break;
                    }
                }
            } else {
                continue;
            }
            
            $Valuta = Valuta::findFirst([
                        "columns" => "id",
                        "conditions" => "num_code = ?1",
                        "bind" => [1 => $item['NumCode']],
                        "bindTypes" => [\Phalcon\Db\Column::BIND_PARAM_STR],
//                "cache" => ["lifetime" => 60 * 60 * 24 * 7, "key" => "valuta-id"],
                        "hydration" => \Phalcon\Mvc\Model\Resultset::HYDRATE_ARRAYS
            ]);

            if ($Valuta) {

                $valuta_value = ValutaValue::findFirstByValutaId($Valuta['id']);

                if (!$valuta_value) {
                    $valuta_value = new ValutaValue();
                }

                $valuta_value->valuta_id = $Valuta['id'];
                $valuta_value->valuta_value = $item['Value'];
                $valuta_value->valuta_nominal = $item['Nominal'];
                $valuta_value->date_update = $date;
                if ($valuta_value->save() == false) {
                    echo "Валюту " . $item['Name'] . " не удалось сохранить/обновить. \n";
                    foreach ($robot->getMessages() as $message) {
                        echo $message, "\n";
                    }
                } else {
                    echo "Валюта " . $item['Name'] . " была сохранена/обновлена. \n";
                }
            }
        }

        $last_update->date = (new \DateTime($date))->format('Y-m-d');
        $last_update->save();
        $this->viewCacheDelete('api');

        $reader->close();
    }
}
