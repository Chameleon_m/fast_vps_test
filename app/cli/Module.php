<?php

namespace FastVps\Cli;

use Phalcon\Mvc\ModuleDefinitionInterface;

/**
 * Cli Module
 */
class Module implements ModuleDefinitionInterface
{

    /**
     * Register a specific autoloader for the module
     */
    public function registerAutoloaders()
    {
        $di = \Phalcon\DI::getDefault();
        $loader = new \Phalcon\Loader();
        $loader->registerNamespaces([
            'FastVps\Cli\Tasks' => __DIR__ . '/tasks/',
            'FastVps\Cli\Models' => __DIR__ . '/models/',
            'FastVps\Common\Models' => $di->get('config')->application->modelsDir,
            'FastVps\Common\Library' => $di->get('config')->application->libraryDir,
            'FastVps\Common\Plugins' => $di->get('config')->application->pluginsDir,
            'Phalcon' => $di->get('config')->application->phalconDir,
        ]);
        $loader->register();
    }

    /**
     * Register specific services for the module
     *
     * @param object $di dependency Injector
     *
     * @return void
     */
    public function registerServices($di)
    {
        /**
         * Read configuration
         */
        $config_module = include __DIR__ . '/config/config.php';

        $di->get('config')->merge($config_module);

        require __DIR__ . '/config/services.php';
    }

}
