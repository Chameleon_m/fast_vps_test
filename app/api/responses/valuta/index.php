<?php

$results = [];
foreach ($items as $item) {
    $results[] = $this->partial('valuta/_item', ['item' => $item]);
}

return [
    'meta' => (object)[
        'number' => count($results),
        'total' => $total,
    ],
    'results' => $results
];

//$i = 0;
//$number = $items->count();
//$results = new SplFixedArray($number);
//
//foreach ($items as $item) {
//    $results[$i++] = $this->partial('valuta/_item', ['item' => $item]);
//}
//
//return [
//    'meta' => (object)[
//        'number' => $number,
//        'total' => $total,
//    ],
//    'results' => $results
//];

//"m": 144944,
//"t": 0.022021055221558

//"m": 141376,
//"t": 0.01357102394104