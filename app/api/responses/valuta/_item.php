<?php

return [
    'id' => $item->id,
    'char_code' => $item->char_code,
    'name_ru' => $item->name_ru,
    'valuta_value' => $item->value->valuta_value,
    'valuta_nominal' => $item->value->valuta_nominal
];

