<?php

namespace FastVps\Api\Controllers;

use FastVps\Api\Models\Valuta;

/**
 * default routers:
 * $this->apiGroup->addOptions ('/{controller}', ['action' => 'options', 'namespace' => $this->namespace]);
 * $this->apiGroup->addGet     ('/{controller}', ['action' => 'index', 'namespace' => $this->namespace]);
 * $this->apiGroup->addPost    ('/{controller}', ['action' => 'post', 'namespace' => $this->namespace]);
 * $this->apiGroup->add        ('/{controller}/{action}', ['namespace' => $this->namespace]);
 * $this->apiGroup->addOptions ('/{controller}/{id' . $this->id_filter . '}', ['action' => 'options', 'namespace' => $this->namespace]);
 * $this->apiGroup->addGet     ('/{controller}/{id' . $this->id_filter . '}', ['action' => 'get', 'namespace' => $this->namespace]);
 * $this->apiGroup->addPut     ('/{controller}/{id' . $this->id_filter . '}', ['action' => 'put', 'namespace' => $this->namespace]);
 * $this->apiGroup->addPatch   ('/{controller}/{id' . $this->id_filter . '}', ['action' => 'patch', 'namespace' => $this->namespace]);
 * $this->apiGroup->addHead    ('/{controller}/{id' . $this->id_filter . '}', ['action' => 'head', 'namespace' => $this->namespace]);
 * $this->apiGroup->addDelete  ('/{controller}/{id' . $this->id_filter . '}', ['action' => 'delete', 'namespace' => $this->namespace]);
 * $this->apiGroup->add        ('/{controller}/{id' . $this->id_filter . '}/{action}', ['namespace' => $this->namespace]);
 */
class ValutaController extends ControllerBase
{
    /**
     * GET /api/valuta
     */
    public function indexAction()
    {
        // Проверяет, кэш на существование или истёкший срок
        if (!$this->view->getCache()->exists($this->cachekey)) {
            $this->view->total = Valuta::count();
            $this->view->items = Valuta::find("status = 'Y'");
        }
        // Кэширование представления этого действия на один день с ключем
        $this->view->cache(["key" => $this->cachekey]);
    }

    /**
     * GET /api/valuta/2 Возвращает валюту по ключу (primary key)
     */
    public function getAction()
    {
        $valuta_id = $this->dispatcher->getParam('id');
//        var_dump($this->view->getCache()->queryKeys());die;
        // Проверяет, кэш на существование или истёкший срок
        if (!$this->view->getCache()->exists($this->cachekey . "_" . $valuta_id)) {
            
            $this->view->item = Valuta::findFirst($valuta_id);
            $this->view->pick('valuta/item');
        }
        // Кэширование представления этого действия на один день с ключем
        $this->view->cache(["key" => $this->cachekey . "_" . $valuta_id]);
    }

}
