<?php

namespace FastVps\Api\Controllers;

use \PhalconRest\Http\Response;

class ErrorController extends ControllerBase
{
    /**
     * Показываем когда не найден controller или action
     */
    public function notFoundAction()
    {
        $code = Response::NOT_FOUND_STATUS_CODE;
        
        $this->response->setStatusCode($code);
        $this->view->message = $this->response->getStatusCodeMessage($code);
        $this->view->code = $code;
    }

}
