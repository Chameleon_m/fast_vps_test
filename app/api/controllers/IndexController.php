<?php

namespace FastVps\Api\Controllers;

class IndexController extends ControllerBase
{

    /**
     * Общая информация
     */
    public function indexAction()
    {
        // Кэширование с использованием настроек по умолчанию
        $this->view->cache(true);
        
    }

}
