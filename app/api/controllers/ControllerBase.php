<?php

namespace FastVps\Api\Controllers;

class ControllerBase extends \Phalcon\Mvc\Controller
{
    use \FastVps\Common\Plugins\Traits\Controller\Cache\KeyBuild;
    
    /**
     * Ключ по умолчанию
     * @var string
     */
    protected $cachekey;

    protected function initialize()
    {
        $this->cachekey = $this->getCacheKey();
    }

}
