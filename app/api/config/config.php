<?php

return new \Phalcon\Config([
    'application' => [
        'baseUri' => '/api',
        'staticBaseUri' => '/',
        'viewsDir' => __DIR__ . '/../responses/',
        'layoutsDir' => '/layouts/',
    ]
]);