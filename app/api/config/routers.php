<?php

namespace FastVps\Api;

class Routers extends \Phalcon\Mvc\Router\Group
{

    public function __construct()
    {
        $this->setPaths([
            'module' => 'api',
            'namespace' => 'FastVps\Api\Controllers',
        ]);

        $this->setPrefix('/api');

        $this->add('/', [
            'controller' => 'index',
            'action' => 'index'
        ]);
    }

}
