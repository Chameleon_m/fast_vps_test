<?php

$di->set('url', function() use ($config_module) {
    $url = new \Phalcon\Mvc\Url();
    $url->setBaseUri($config_module->application->baseUri);
    $url->setStaticBaseUri($config_module->application->staticBaseUri);
    return $url;
}, true);

$di->set('dispatcher', function() use ($di) {
    //Create/Get an EventManager ~ Observer
    $eventsManager = new \Phalcon\Events\Manager();

    //Instantiate the Security plugin
    $security = new \FastVps\Common\Plugins\SecurityAjaxRequest($di);

    //Listen for events produced in the dispatcher using the Security plugin
    $eventsManager->attach('dispatch:beforeDispatch', $security);

    //Attach a listener
    $eventsManager->attach('dispatch:beforeException', function($event, $dispatcher, $exception) {
        //controller or action doesn't exist
        if ($event->getType() == 'beforeException') {
            switch ($exception->getCode()) {
                case \Phalcon\Mvc\Dispatcher::EXCEPTION_HANDLER_NOT_FOUND:
                case \Phalcon\Dispatcher::EXCEPTION_ACTION_NOT_FOUND:
                    $dispatcher->forward([
                        'module' => 'api',
                        'controller' => 'error',
                        'action' => 'notFound'
                    ]);
                    return false;
            }
        }
    });

    $dispatcher = new \Phalcon\Mvc\Dispatcher();
    $dispatcher->setDefaultNamespace("FastVps\Api\Controllers");
    //Bind the EventsManager to the dispatcher
    $dispatcher->setEventsManager($eventsManager);

    return $dispatcher;
});

$di->set('jsonEngine', function($view, $di) {
    $engine = new \PhalconRest\Mvc\View\Engine\Json($view, $di);
    $engine
            ->setJsonEncodeOptions(JSON_UNESCAPED_UNICODE | JSON_BIGINT_AS_STRING | JSON_PRETTY_PRINT)
            ->setJsonpContentType('application/javascript')
            ->setJsonContentType('application/json');

    return $engine;
});

$di->set('view', function() use ($config_module) {
    $restView = new \PhalconRest\Mvc\RestView();
    $restView->disableLevel([
        \PhalconRest\Mvc\RestView::LEVEL_AFTER_TEMPLATE => true,
        \PhalconRest\Mvc\RestView::LEVEL_BEFORE_TEMPLATE => true,
        \PhalconRest\Mvc\RestView::LEVEL_MAIN_LAYOUT => true,
        \PhalconRest\Mvc\RestView::LEVEL_LAYOUT => true
    ]);
    $restView->setViewsDir($config_module->application->viewsDir);
    $restView->registerEngines(['.php' => 'jsonEngine']);
    return $restView;
});

/**
 * Назначение сервиса кэширования представлений
 */
$di->set('viewCache', function () use ($di) {

    if (DEVELOPMENT) {
        $frontCache = new \Phalcon\Cache\Frontend\None();
        return new \Phalcon\Cache\Backend\Memory($frontCache);
    } else {
        // Кэширование данных данных из стандартного PHP вывода
        $frontCache = new \Phalcon\Cache\Frontend\Output([
            'lifetime' => $di->get('config')->cache->lifetime
        ]);

        $cache = new \Phalcon\Cache\Backend\File($frontCache, [
            'cacheDir' => $di->get('config')->cache->cacheDir->view,
            'prefix' => $di->get('config')->cache->prefixModules->api
        ]);

        return $cache;
    }
});

$di->set('response','\PhalconRest\Http\Response');

require_once __DIR__ . '/routers.php';

$rest = new \PhalconRest\Mvc\Router\Rest();
$rest
        ->setNamespace('FastVps\Api\Controllers')
        ->setPrefix($config_module->application->baseUri)
        ->setIdFilter('[0-9]+')
        ->init()
        ->mountTo($di->get('router'));

$di->get('router')->mount(new \FastVps\Api\Routers())->handle();
