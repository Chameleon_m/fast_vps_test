<?php

namespace FastVps\Backend;

use Phalcon\Mvc\ModuleDefinitionInterface;

class Module implements ModuleDefinitionInterface
{

    public function registerAutoloaders($di)
    {

        $loader = new \Phalcon\Loader();

        $loader->registerNamespaces([
            'FastVps\Backend\Controllers' => __DIR__ . '/controllers/',
            'FastVps\Backend\Models' => __DIR__ . '/models/',
            'FastVps\Common\Models' => $di->get('config')->application->modelsDir,
            'FastVps\Common\Library' => $di->get('config')->application->libraryDir,
            'FastVps\Common\Forms' => $di->get('config')->application->formsDir,
            'FastVps\Common\Plugins' => $di->get('config')->application->pluginsDir,
            'Phalcon' => $di->get('config')->application->phalconDir,
        ]);

        $loader->register();
    }

    public function registerServices($di)
    {
        /**
         * Read configuration
         */
        $config_module = include __DIR__ . "/config/config.php";

        $di->get('config')->merge($config_module);

        require __DIR__ . '/config/services.php';
    }

}
