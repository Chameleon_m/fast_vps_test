<?php

namespace FastVps\Backend;

class Routers extends \Phalcon\Mvc\Router\Group {

    public function __construct() {
        $this->setPaths([
            'module' => 'backend',
            'namespace' => 'FastVps\Backend\Controllers',
        ]);
        
        $this->setPrefix('/admin');
        
        $this->add('/:controller', ['module' => 'backend','controller' => 1, 'action' => 'index']);
        $this->add('/:controller/:action', ['module' => 'backend','controller' => 1, 'action' => 2]);
        $this->add('/:controller/:action/:int', ['module' => 'backend','controller' => 1, 'action' => 2, 'id' => 3]);
    }

}
