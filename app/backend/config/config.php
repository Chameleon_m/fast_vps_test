<?php

return new \Phalcon\Config([
    'application' => [
        'baseUri' => '/admin',
        'baseUriFontend' => '/',
        'staticBaseUri' => '/',
        'viewsDir' => __DIR__ . '/../views/',
        'layoutsDir' => '/layouts/',
    ]
]);