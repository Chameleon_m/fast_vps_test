<?php

$di->set('url', function() use ($config_module) {
    $url = new \Phalcon\Mvc\Url();
    $url->setBaseUri($config_module->application->baseUri);
    $url->setStaticBaseUri($config_module->application->staticBaseUri);
    return $url;
}, true);

$di['dispatcher'] = function() {
    $dispatcher = new \Phalcon\Mvc\Dispatcher();
    $dispatcher->setDefaultNamespace("FastVps\Backend\Controllers");
    return $dispatcher;
};

$di->set('view', function() use ($di) {

    $view = new \Phalcon\Mvc\View();
    $view->setViewsDir($di->get('config')->application->viewsDir);
    $view->setLayoutsDir($di->get('config')->application->layoutsDir);
    $view->setTemplateAfter('main');

    $view->registerEngines([
        '.phtml' => function($view, $di) {

            $volt = new \Phalcon\Mvc\View\Engine\Volt($view, $di);
            $volt->setOptions([
                'compiledPath' => $di->get('config')->cache->cacheDir->volt,
                'compiledSeparator' => '_',
                'stat' => true, // Проверяет существуют ли различия между файлом шаблона и его скомпилированным результатом
                //'compileAlways' => true  //шаблоны компилируются с учётом изменений родительского шаблона.
            ]);

            return $volt;
        }
    ]);

    return $view;
}, true);

require_once __DIR__ . '/routers.php';
$di->get('router')->mount(new \FastVps\Backend\Routers())->handle();
