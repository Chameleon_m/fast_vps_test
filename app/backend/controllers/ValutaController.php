<?php

namespace FastVps\Backend\Controllers;

use FastVps\Backend\Models\Valuta;
use FastVps\Backend\Models\ValutaValue;
use FastVps\Common\Forms\ValutaForm;

class ValutaController extends ControllerBase
{
    use \FastVps\Common\Plugins\Traits\View\ClearCache;

    public function indexAction()
    {
        $this->view->items = Valuta::find([
                    "order" => "default_active,status"
        ]);
    }

    public function addAction()
    {
        $form = new ValutaForm();

        if ($this->request->isPost()) {

            if ($form->isValid($this->request->getPost()) === true) {

                $item = new Valuta();
                $item->valute_attr_id = $this->request->getPost('valute_attr_id');
                $item->num_code = $this->request->getPost('num_code');
                $item->char_code = $this->request->getPost('char_code');
                $item->name_ru = $this->request->getPost('name_ru');
                $item->default_active = $this->request->getPost('default_active');
                $item->status = Valuta::NOT_DELETED;

                $item_value = new ValutaValue();
                $item_value->valuta_id = $item->id;
                $item_value->valuta_nominal = $this->request->getPost('valuta_nominal');
                $item_value->valuta_value = $this->request->getPost('valuta_value');
                $item_value->date_update = date('Y-m-d');

                $item->value = $item_value;

                if (!$item->create()) {
                    foreach ($item->getMessages() as $message) {
                        $this->flashSession->error($message);
                    }
                    return $this->response->redirect('/admin/valuta', true);
                }

                $this->flashSession->success("Валюта добавлена");

                $this->viewCacheDelete();

                return $this->response->redirect('/admin/valuta', true);
            } else {
                foreach ($form->getMessages() as $message) {
                    $this->flash->error($message);
                }
            }
        }
        $this->view->form = $form;
    }

    public function editAction()
    {
        $id = $this->dispatcher->getParam('id');
        
        $item = Valuta::findFirst($id);        
        $item_related = $item->getValue();        
        $item->valuta_nominal = $item_related->valuta_nominal;
        $item->valuta_value = $item_related->valuta_value;

        $form = new ValutaForm($item);

        if ($this->request->isPost()) {

            if ($form->isValid($this->request->getPost()) != false) {
                
                $item->valute_attr_id = $this->request->getPost('valute_attr_id');
                $item->num_code = $this->request->getPost('num_code');
                $item->char_code = $this->request->getPost('char_code');
                $item->name_ru = $this->request->getPost('name_ru');
                $item->default_active = $this->request->getPost('default_active');
                $item->status = Valuta::NOT_DELETED;
                
//                $item_related->valuta_id = $item->id;
                $item_related->valuta_nominal = $this->request->getPost('valuta_nominal');
                $item_related->valuta_value = $this->request->getPost('valuta_value');
                $item_related->date_update = date('Y-m-d');

                $item->value = $item_related;
                if (!$item->update()) {
                    foreach ($item->getMessages() as $message) {
                        $this->flashSession->error($message);
                    }
                    return $this->response->redirect('/admin/valuta', true);
                }                

                $this->flashSession->success("Валюта успешно отредактирована");

                $this->viewCacheDelete();

                return $this->response->redirect('/admin/valuta', true);

            }else{
                foreach ($form->getMessages() as $message) {
                        $this->flash->error($message);
                }
            }

        }

        $this->view->form = $form;
    }

    public function deleteAction()
    {
        $id = $this->dispatcher->getParam('id');
                
        $item = Valuta::findFirst($id);
        if ($item != false) {
            if ($item->delete() === false) {
                foreach ($item->getMessages() as $message) {
                    $this->flash->error($message);
                }
            } else {
                $this->flashSession->success("Валюта успешно удалена");
                $this->viewCacheDelete();
            }
        }
        
        return $this->response->redirect('/admin/valuta', true);
    }

}
