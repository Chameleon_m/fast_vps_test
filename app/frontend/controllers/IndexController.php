<?php

namespace FastVps\Frontend\Controllers;

use FastVps\Frontend\Models\Valuta;

class IndexController extends ControllerBase
{

    public function indexAction()
    {
        \Phalcon\Tag::prependTitle("Главная страница");
        $this->assets->addJs('js/valuta_update.js', true);

        $this->view->left_menu = Valuta::find([
                    "status = 1",
                    "cache" => ["key" => $this->cachekey]
        ]);
    }

}
