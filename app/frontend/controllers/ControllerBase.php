<?php

namespace FastVps\Frontend\Controllers;

class ControllerBase extends \Phalcon\Mvc\Controller
{
    use \FastVps\Common\Plugins\Traits\Controller\Cache\KeyBuild;

    public $cachekey;

    protected function initialize()
    {
        \Phalcon\Tag::prependTitle(' | Курсы валют ');
        $this->cachekey = $this->getCacheKey();
    }

}
