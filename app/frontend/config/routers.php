<?php
namespace FastVps\Frontend;

class Routers extends \Phalcon\Mvc\Router\Group {

    public function __construct() {

        $this->setPaths([
            'module' => 'frontend',
            'namespace' => 'FastVps\Frontend\Controllers',
        ]);

        $this->add('/', [
            'controller' => 'index',
            'action' => 'index'
        ]);

        $this->add('/:controller', [
            'controller' => 1,
            'action' => 'index'
        ]);

        $this->add('/:controller/:action', [
            'controller' => 1,
            'action' => 2
        ]);

        $this->add('/:controller/:action/:params', [
            'controller' => 1,
            'action' => 2,
            'params' => 3
        ]);
    }

}
