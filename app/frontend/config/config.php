<?php

return new \Phalcon\Config([
    'application' => [
        'baseUri' => '/',
        'staticBaseUri' => '/',
        'viewsDir' => __DIR__ . '/../views/',
        'layoutsDir' => '/layouts/',
    ]
]);
