<?php

date_default_timezone_set($config->application->timezone);

if (APPLICATION_ENV === 'dev') {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    define('DEVELOPMENT', TRUE );

} else {
//    error_reporting(E_ALL & ~E_NOTICE);
    error_reporting(E_ALL);
    ini_set('display_errors', 0);
    define('DEVELOPMENT', FALSE );
}

