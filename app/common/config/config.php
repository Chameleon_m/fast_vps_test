<?php

return new \Phalcon\Config([
    'application' => [
        'modelsDir' => __DIR__ . '/../models/',
        'formsDir' => __DIR__ . '/../forms/',
        'libraryDir' => __DIR__ . '/../library/',
        'pluginsDir' => __DIR__ . '/../plugins/',
        'phalconDir' => __DIR__ . '/../library/Phalcon/',
        'logsDir' => __DIR__ . '/../logs/',
        'publicUrl' => '92.222.68.125',
//        'cryptSalt' => '$9diko$.f#11',
//        'securitycrypt' => \Phalcon\Security::CRYPT_MD5,
//        'securitykey'   => '285c65aae106302ffba3ce3a21dab3f1',
//        'securityWorkFactor' => 12
        'timezone' => 'Europe/Moscow',
    ],
    'database' => [
        'adapter' => 'Mysql',
        'host' => 'localhost',
        'username' => 'root',
        'password' => '',
        'dbname' => 'fast_vps',
        'charset' => 'utf8'
    ],
    'database_log' => false,
    'database_log_path' => __DIR__ . '/../logs/db_' . date('Y-m-d') . '.log', 
    
    'cache' => [
        'cacheDir' => [
            'view'      => __DIR__ . '/../cache/view/',
            'volt'      => __DIR__ . '/../cache/volt/',
            'metaData'  => __DIR__ . '/../cache/metaData/',
            'models'    => __DIR__ . '/../cache/models/',
        ],
        
        'lifetime' => 86400, // 60 * 60 * 24
        
        'prefixModules' => [
            'api' => 'api',
            'frontend' => 'frontend',
            'backend' => 'backend',
            'cli' => 'cli'
        ]
    ]
]);
