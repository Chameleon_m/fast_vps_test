<?php

/**
 * The FactoryDefault Dependency Injector automatically register the right 
 * services providing a full stack framework
 */
$di = new \Phalcon\DI\FactoryDefault();

/**
 * Read the configuration
 */
$config = include APPLICATION_PATH . '/common/config/config.php';

include APPLICATION_PATH . '/common/config/environment.php';

/**
 * Register the global configuration as config
 */
$di->set('config', $config);

/**
 * Main logger file
 */
$di->set('logger', function() use ($config) {
    return new \Phalcon\Logger\Adapter\File($config->application->logsDir . date('Y-m-d') . '.log');
}, true);

$di->set('router', function () {
    return include APPLICATION_PATH . '/common/config/routers.php';
}, true);

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->set('db', function() use ($config) {
    
    if ($config->database_log || DEVELOPMENT) {
        
        $connection = new \Phalcon\Db\Adapter\Pdo\Mysql($config->database->toArray());
        $eventsManager = new \Phalcon\Events\Manager();
        // Создание слушателя базы данных
        $dbListener = new \FastVps\Common\Plugins\DbLog($config->database_log_path);
        // Слушать все события базы данных
        $eventsManager->attach('db', $dbListener);
        // Совмещение менеджера событий с адаптером базы данных
        $connection->setEventsManager($eventsManager);
        
    } else {
        $connection = new \Phalcon\Db\Adapter\Pdo\Mysql($config->database->toArray());
    }
    
    return $connection;
});

$di->set('modelsManager','Phalcon\Mvc\Model\Manager');

//$di->setShared('transactions', function(){
//    return new \Phalcon\Mvc\Model\Transaction\Manager();
//});

$di->set('modelsMetadata', function() use ($config) {

    if (DEVELOPMENT) return new \Phalcon\Mvc\Model\Metadata\Memory();

    return new \Phalcon\Mvc\Model\Metadata\Files([
        'metaDataDir' => $config->cache->cacheDir->metaData
    ]);
}, true);

$di->set('modelsCache', function() use ($config) {
        if (DEVELOPMENT) {
            $frontCache = new \Phalcon\Cache\Frontend\None();
            return new Phalcon\Cache\Backend\Memory($frontCache);
        } else {
            $frontCache = new \Phalcon\Cache\Frontend\Data([
                "lifetime" => $config->cache->lifetime
            ]);

            return new \Phalcon\Cache\Backend\File($frontCache, [
                "cacheDir" => $config->cache->cacheDir->models
            ]);
        }
    }
);

$di->set('session', function() {
    $session = new \Phalcon\Session\Adapter\Files();
    $session->start();
    return $session;
});

$di->set('cookies', function () {
    $cookies = new \Phalcon\Http\Response\Cookies();
    $cookies->useEncryption(false);
    return $cookies;
}, true);

/* Crypt service */
//$di->set('crypt', function() use ($config) {
//    $crypt = new Crypt();
//    $crypt->setKey($config->application->cryptSalt);
//    return $crypt;
//});

//$di->set('security', function() use ($config) {
//    $security = new \Phalcon\Security();
//    //Устанавливаем фактор хеширования в 12 раундов
//    $security->setWorkFactor($config->application->securityWorkFactor);
//
//    return $security;
//}, true);

/**
 * Менеджер форм
 */
//$di->set('forms', function () {
//    return new \Phalcon\Forms\Manager();
//}, true);

$di->set('filter', function () {
    $filter = new \Phalcon\Filter();
    $filter->add('numbers', new \Phalcon\Filter\UserFilter\NumbersFilter);
    return $filter;
});