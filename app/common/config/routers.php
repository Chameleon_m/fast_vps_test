<?php

/**
 * Initialize default router and mount backend & frontend routers
 */
$router = new \Phalcon\Mvc\Router(false);

$router->setUriSource(\Phalcon\Mvc\Router::URI_SOURCE_SERVER_REQUEST_URI);

$module = 'frontend';
if (isset($_SERVER['REQUEST_URI'])) {
    if (preg_match('#^/api#', $_SERVER['REQUEST_URI'])) $module = 'api';
    elseif (preg_match('#^/admin#', $_SERVER['REQUEST_URI'])) $module = 'backend';
}

$router->setDefaultModule($module);

// Delete last slashes
$router->removeExtraSlashes(true);

return $router;
