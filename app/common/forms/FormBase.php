<?php

namespace FastVps\Common\Forms;

abstract class FormBase extends \Phalcon\Forms\Form {
    
    /**
     * Печать сообщения для конкретного элемента
     * 
     * @param string $name название элемента
     */
    public function messages($name)
    {
        if($this->hasMessagesFor($name)) {
            foreach($this->getMessagesFor($name) as $message) {
                $this->flash->error($message);
            }
        }
    }
    
}
