<?php

namespace FastVps\Common\Forms;

use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Numeric;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Regex;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Between;

class ValutaForm extends FormBase
{

    public function initialize($entity = null, $options = null)
    {
        $valute_attr_id = new Text('valute_attr_id', [
            'autofocus' => true,
            'maxlength' => 7,
            'required' => true,
            'pattern' => '[A-Za-z0-9]{1,7}'
        ]);

        $valute_attr_id->addValidators([
            new PresenceOf([
                'message' => 'Укажите идентификатор валюты с источника получения'
            ]),
            new StringLength([
                'max' => 7,
                'messageMaximum' => "Идентификатор валюты должен быть не более 7 символов"
            ]),
        ]);

        $num_code = new Text('num_code', [
            'maxlength' => 3,
            'required' => true,
            'pattern' => '\d{3}'
        ]);
        $num_code->addValidators([
            new PresenceOf([
                'message' => 'Укажите цифровой код валюты'
            ]),
            new Regex([
                'pattern' => '/\d{3}/',
                'message' => 'Цифровой код должен состоять из 3 цифр'
            ])
        ]);

        $char_code = new Text('char_code', [
            'maxlength' => 3,
            'required' => true,
            'pattern' => '[A-Z]{3}'
        ]);
        $char_code->addValidators([
            new PresenceOf([
                'message' => 'Укажите буквенный код валюты'
            ]),
            new Regex([
                'pattern' => '/[A-Z]{3}/',
                'message' => 'Буквенный код должен состоять из 3 заглавных букв'
            ])
        ]);


        $name_ru = new Text('name_ru', [
            'maxlength' => 255,
            'required' => true,
            'pattern' => '[А-Яа-яЁё\s]{3,255}'
        ]);
        $name_ru->addValidators([
            new PresenceOf([
                'message' => 'Укажите наименование валюты (на русском)'
            ]),
            new StringLength([
                'min' => 3,
                'max' => 255,
                'messageMinimum' => "Наименование валюты должно состоять минимум из 3 букв",
                'messageMaximum' => "Наименование валюты должно состоять максимум из 255 букв"
            ])
        ]);


//        $name_en = new Text('name_en');

        $default_active = new Select('default_active', [
            'Y' => 'Да',
            'N' => 'Нет'
        ]);

        $status = new Select('status', [
            'Y' => 'В списке',
            'N' => 'Вне списка'
        ]);
        
        $valuta_value = new Text('valuta_value', [
            'maxlength' => 255,
            'required' => true,
            'pattern' => '\d{2},\d{4}'
        ]);
        $valuta_value->addValidators([
            new PresenceOf([
                'message' => 'Укажите курс валюты'
            ]),
            new Regex([
                'pattern' => '/\d{2},\d{4}/',
                'message' => 'Значение курса должно соответствовать шаблону **,****'
            ])
        ]);
        
        $valuta_nominal = new Select('valuta_nominal', [
            1 => 1,
            10 => 10,
            100 => 100,
            1000 => 1000,
            10000 => 10000,
            100000 => 100000,
            1000000 => 1000000,
            10000000 => 10000000
        ]);
        $valuta_nominal->addValidators([
            new PresenceOf([
                'message' => 'Укажите номинал валюты'
            ]),
            new Regex([
                'pattern' => '/1{1}0{0,7}/',
                'message' => 'Номинал должен быть указан в диапазоне от 1 до 10000000 с ведущей единицой'
            ]),
//            new Between([
//                'minimum' => 1,
//                'maximum' => 10000000,
//                'message' => 'Номинал должен быть указан в диапазоне от 1 до 10000000'
//            ])
        ]);

        $this->add(new Submit('Checkout', [
            'value' => 'Отправить'
        ]));

        $this
                ->add($valute_attr_id)
                ->add($num_code)
                ->add($char_code)
                ->add($name_ru)
//                ->add($name_en)
                ->add($default_active)
                ->add($status)
                ->add($valuta_value)
                ->add($valuta_nominal);
    }

}
