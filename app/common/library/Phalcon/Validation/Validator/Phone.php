<?php

namespace Phalcon\Validation\Validator;

use Phalcon\Validation\Validator;
use Phalcon\Validation\ValidatorInterface;
use Phalcon\Validation\Message;

class Phone extends Validator implements ValidatorInterface
{

    private static $_message = [];

    /**
     * Выполнение валидации
     *
     * @param Phalcon\Validation $validator
     * @param string $attribute
     * @return boolean
     */
    public function validate($validator, $attribute)
    {
        $value = $validator->getValue($attribute);

        if (!self::international($value)) {
            $message = $this->getOption('message');
            if (!$message) {
                foreach (self::$_message as $mes) {
                    $validator->appendMessage(new Message($mes, $attribute, 'Phone'));
                }
            } else {
                $validator->appendMessage(new Message($message, $attribute, 'Phone'));
            }

            return false;
        }

        return true;
    }

    /**
     * 
     * @param string $number
     * @param mixed $lengths
     * @return boolean
     */
    public static function low($number, $lengths = NULL)
    {
        if (!is_array($lengths)) {
            $lengths = array(7, 10, 11);
        }

        // Удалит все нецифровые символы
        $number = preg_replace('/\D+/', '', $number);

        // Проверит, подходит ли кол-во цифр
        return in_array(strlen($number), $lengths);
    }

    /**
     * 
     * @param string $number
     * @param mixed $lengths
     * @return boolean
     */
    public static function low_ru($number)
    {
        // Удалит все нецифровые символы
        $number = preg_replace('/\D+/', '', $number);

        if (strlen($number) !== 11) {
            self::$_message[] = "Формат телефона указан неверно. Номер должен содержать 11 цифр";
        }
        if (substr($number, 0, 1) != 7) {
            self::$_message[] = "Номер должен начинаться с 7 (Телефонный код РФ)";
        }

        return empty(self::$_message) ? true : false;
    }

    /**
     * Проверяет соответствие номера телефона международному формату
     * 
     * @param string $number
     * @return string|boolean
     */
    public static function international($number)
    {
        //'/^(\+)?[\s*]?(\d{1,3})?[\s*]?\((\d{2,5})?\)[\s*]?([\d-\s]*)/'
        if (preg_match('/^\+?[\s*]?(\d{1,3})?[\s*]?\((\d{2,5})?\)[\s*]?([\d-\s]*)/', $number, $number_parts)) {

            list(
//                $plus,
                $country, $operator_or_city, $phone) = $number_parts;

            $plus = '';
//            if(!$plus){
//                self::$_message[] =  "Формат телефона указан неверно. Номер должен начинаться с \"+\"";
//            }

            if (!$country) {
                self::$_message[] = "Формат телефона указан неверно. Укажите код страны";
            }

            if (!$operator_or_city) {
                self::$_message[] = "Формат телефона указан неверно. Укажите код оператора";
            }

            if (!$phone) {
                self::$_message[] = "Формат телефона указан неверно. Укажите ваш номер";
            } else {
                $phone = preg_replace('/\D+/', '', $phone);
            }



            return empty(self::$_message) ? $plus . $country . $operator_or_city . $phone : false;
        }

        return false;
    }

}
