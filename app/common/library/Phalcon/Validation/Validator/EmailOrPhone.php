<?php

namespace Phalcon\Validation\Validator;

use Phalcon\Validation\Validator;
use Phalcon\Validation\ValidatorInterface;
use Phalcon\Validation\Message;
use Phalcon\Validation\Validator\Email;

class EmailOrPhone extends Validator implements ValidatorInterface
{

    /**
     * Выполнение валидации
     *
     * @param Phalcon\Validation $validator
     * @param string $attribute
     * @return boolean
     */
    public function validate($validator, $attribute)
    {
        $value = $validator->getValue($attribute);

        if (strpos($value, '@')) {
            $validator_class = new Email([
                'message' => "E-mail ввдён неверно"
            ]);
        } else {
            $validator_class = new Phone();
        }

        $messages = $validator_class->validate($validator, $attribute);

        if (!$messages) {
//                foreach ($messages as $message) {
//                    $validator->appendMessage(new Message($message, $attribute, 'EmailOrPhone'));
//                }
            $validator->appendMessage(new Message('Телефон или email ввдён неверно', $attribute, 'EmailOrPhone'));

            return false;
        }

        return true;
    }

}
