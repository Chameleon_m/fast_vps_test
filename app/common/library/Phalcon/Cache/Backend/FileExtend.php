<?php

namespace Phalcon\Cache\Backend;

class FileExtend extends \Phalcon\Cache\Backend\File
{

    public function flush()
    {
        // Удаляем все из кэша
        $keys = $this->queryKeys($this->_prefix);
        $this->_prefix = '';
        if (empty($keys)) return parent::flush();

        foreach ($keys as $key) {
            $this->delete($key);
        }
        
        return true;
    }

}
