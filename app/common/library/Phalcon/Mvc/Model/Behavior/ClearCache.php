<?php
namespace Phalcon\Mvc\Model\Behavior;

use Phalcon\Mvc\Model\Behavior;
use Phalcon\Mvc\Model\BehaviorInterface;
/**
 * @todo доработать чтоб удаляло только связанный кэш
 */
class ClearCache extends Behavior implements BehaviorInterface
{
    public function notify($eventType, $model)
    {
        switch ($eventType) {

            case 'afterCreate':
            case 'afterDelete':
            case 'afterUpdate':
                /**
                 *  Чистим кеш 
                 */
                $modelsCache = $model->getDI()->get('modelsCache');
                if($modelsCache instanceof \Phalcon\Cache\BackendInterface){
                    $modelsCache->flush();
                }
                
                break;
            /* игнорировать остальную часть событий */
            default:
                break;
                
        }
    }
    
    

}