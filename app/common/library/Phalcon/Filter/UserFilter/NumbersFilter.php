<?php

namespace Phalcon\Filter\UserFilter;

use Phalcon\Filter\UserFilterInterface;

/**
 * 
 * Удаляет все нецифровые символы. regex [0-9]
 */

class NumbersFilter implements UserFilterInterface
{

    public function filter($value)
    {
        return preg_replace('/\D+/', '', $value);
    }

}