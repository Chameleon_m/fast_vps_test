<?php

namespace FastVps\Common\Models;

class Base extends \Phalcon\Mvc\Model
{

    public $module_name;

    public function initialize()
    {
        /**
         * При сохранении обновленного объекта по умолчанию в UPDATE-запрос
         * собирает все поля, даже неизмененные. Чтоб не гонять лишние данные,
         * включаешь dynamic update - и отправляет только те, которые изменились.
         */
        $this->useDynamicUpdate(true);

        /**
         * Тут мы определяем с какого модуля идет обращение к общей модели
         * А в дочереней модели на основе этой переменной проставляем связи с моделями в модуле
         */
        $this->module_name = ucfirst(parent::getDI()->getRouter()->getModuleName());
    }

}

/**
 * Перевод и приведение к нужному виду родительского метода getMessages
 *
 * @return array
 */
//    public function getMessages()
//    {
//        $messages = array();
//        foreach (parent::getMessages() as $message) {
//            /**
//             * @see \Phalcon\Mvc\Model\Message
//             * @link http://docs.phalconphp.com/ru/latest/api/Phalcon_Mvc_Model_Message.html
//             *
//             * $messages[] = new Message($text, $field, $type, $code); как вариант
//             */
//            switch ($message->getType()) {
//                case 'InvalidCreateAttempt':
//                    $messages[] = 'Запись в ' . $message->getModel() .' не может быть создана, потому что уже существует - код ошибки: ' . $message->getCode();
//                    break;
//                case 'InvalidUpdateAttempt':
//                    $messages[] = 'Запись в ' . $message->getModel() .' не может быть обновлена, поскольку не существует - код ошибки: ' . $message->getCode();
//                    break;
//                case 'PresenceOf':
//                    $messages[] = 'Поле ' . $message->getField() . 'в ' . $message->getModel() .' обязательно к заполнению - код ошибки: ' . $message->getCode();
//                    break;
//                case 'ConstraintViolation':
//                    $messages[] = 'Поле ' . $message->getField() . ' является частью виртуального внешнего ключа, была попытка создать/обновить значение, которое не существует в относящиеся модели - код ошибки: ' . $message->getCode();
//                    break;
//                case 'InvalidValue':
//                    $messages[] = 'Недопустимое значение поля ' . $message->getField() . ' в ' . $message->getModel() .' - код ошибки: ' . $message->getCode();
//                    break;
//            }
//        }
//        return $messages;
//    }

/*
 *  Срабатует при неудачном создании записи или обновлении
 */
//    public function notSave()
//    {
//        //Obtain the flash service from the DI container
//        $flash = $this->getDI()->getFlash();
//
//        //Show validation messages
//        foreach ($this->getMesages() as $message) {
//            $flash->error($message);
//        }
//    }

/*
 * Срабатует при ошибке валидации
 */
//    public function onValidationFails(){
//
//    }

/*
 * Срабатывает перед валидацией
 */
//    public function beforeValidation(){
//
//    }

/*
 * Поменяет флаг удаления
 * http://docs.phalconphp.com/ru/latest/reference/models.html#using-traits-as-behaviors
 */
//    public function initialize()
//    {
//        $this->addBehavior(new SoftDelete(
//            array(
//                'field' => 'status',
//                'value' => self::NOT
//            )
//        ));
//    }
