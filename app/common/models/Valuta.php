<?php

namespace FastVps\Common\Models;

use FastVps\Common\Models\Base;
use Phalcon\Mvc\Model\Behavior\SoftDelete;
use Phalcon\Mvc\Model\Behavior\ClearCache;

/**
 * Данные валюты СБР
 */
class Valuta extends Base
{

    /**
     * Для SoftDelete
     */
    const DELETED = 'N';
    const NOT_DELETED = 'Y';

    /**
     * Primary Key
     * @var integer
     */
    public $id;

    /**
     * ID валюты в СБР
     * @var string 
     */
    public $valute_attr_id;

    /**
     * Цифровой код
     * @var string
     */
    public $num_code;

    /**
     * Буквенный код
     * @var string
     */
    public $char_code;

    /**
     * Наименование валюты на русском
     * @var string
     */
    public $name_ru;

    /**
     * Наименование валюты на английском
     * @var string
     */
    public $name_en;

    /**
     * Статус активности валюты по умолчанию.
     * ENUM('Y','N')
     * 
     * @var string
     */
    public $default_active;

    /**
     * Статус валюты.
     * ENUM('Y','N')
     * 
     * @var string
     */
    public $status;

    /**
     * Установка отношений и другим параметров для модели
     */
    public function initialize()
    {
        parent::initialize();
        /**
         *  1 к 1
         * @link http://docs.phalconphp.ru/ru/latest/reference/models.html#id12
         */
        $this->hasOne(
            'id',
            'FastVps\\' . $this->module_name . '\\Models\\ValutaValue',
            'valuta_id',
            [
                'alias' => 'value',
//                'foreignKey' => [
//                    // удалит все относящиеся записи, если основная запись удаляется.
//                    'action' => \Phalcon\Mvc\Model\Relation::ACTION_CASCADE
//                ]
            ]
        );

        /**
         * @link http://docs.phalconphp.ru/ru/latest/reference/models.html#id34 Behavior
         * @link http://docs.phalconphp.ru/ru/latest/reference/models.html#softdelete SoftDelete
         */
//        $this->addBehavior(new SoftDelete([
//            'field' => 'status',
//            'value' => self::DELETED
//        ]));
        
        $this->addBehavior(new ClearCache());
    }

    /**
     * Вернуться соответствующую запись "value"
     *
     * @link http://docs.phalconphp.ru/ru/latest/reference/models.html#id15
     * @return \FastVps\Common\Models\ValutaValue[]
     */
    public function getValue($parameters = null)
    {
        return $this->getRelated('value', $parameters);
    }

}
