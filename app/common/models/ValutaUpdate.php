<?php

namespace FastVps\Common\Models;

use FastVps\Common\Models\Base;

/**
 * Время последнего запроса обновления Валюты СБР
 */
class ValutaUpdate extends Base
{

    public $id;
    public $date;

}
