<?php

namespace FastVps\Common\Models;

use FastVps\Common\Models\Base;
use Phalcon\Mvc\Model\Behavior\Timestampable;

/**
 * Курсы и наминал валют
 */
class ValutaValue extends Base
{

    /**
     * Primary Key
     * @var integer
     */
    public $id;

    /**
     * ID валюты
     * @var integer
     */
    public $valuta_id;

    /**
     * Курс
     * @var string
     */
    public $valuta_value;

    /**
     * Наминал
     * @var integer
     */
    public $valuta_nominal;

    /**
     * Дата актуальности курса
     * @var string
     */
    public $date_update;

    public function initialize()
    {
        parent::initialize();
        
        $this->hasOne(
            'valuta_id',
            'FastVps\\' . $this->module_name . '\\Models\\Valuta',
            'id',
            [
                'alias' => 'valuta'
            ]
        );
        
        
        /**
         * @link http://docs.phalconphp.ru/ru/latest/reference/models.html#timestampable
         */
//        $this->addBehavior(new Timestampable([
//            'beforeCreate' => [
//                'field' => 'date_update',
//                'format' => 'Y-m-d'
//            ]
//        ]));
    }
    
//    public function beforeSave()
//    {
//        $this->date_update = date('Y-m-d');
//    }

}
