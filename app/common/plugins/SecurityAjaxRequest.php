<?php

namespace FastVps\Common\Plugins;

use Phalcon\Events\Event;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\User\Plugin;

class SecurityAjaxRequest extends Plugin
{
    const HEADER_NAME = 'XSRFTOKEN';

    public function beforeDispatch(Event $event, Dispatcher $dispatcher)
    {
        // Проверим, что это был Ajax-запрос
        if ($this->getDI()->get('request')->isAjax() === false) {            
            $this->getDI()->get('response')->setStatusCode(404, "Not Found")->sendHeaders();
            $event->stop();
            return false;
        }
        // Сраниваем токены 
        if($this->getDI()->get('security')->getSessionToken() !== $this->getDI()->get('request')->getHeader(self::HEADER_NAME)){
            $this->getDI()->get('response')->setStatusCode(404, "Not Found")->sendHeaders();
            $event->stop();
            return false;
        }
    }

}
