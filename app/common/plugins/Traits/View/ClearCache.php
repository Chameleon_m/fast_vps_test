<?php

namespace FastVps\Common\Plugins\Traits\View;

trait ClearCache
{

    public function viewCacheDelete($prefix = null)
    {
        if(is_array($prefix)){
            foreach ($prefix as $value) {
                $this->viewCacheDelete($value);
            }
        }
        
        $options = [
            'cacheDir' => $this->getDI()->get('config')->cache->cacheDir->view
        ];
        
        if(!is_null($prefix)){
            if(!is_string($prefix))
                throw new \Exception('Префикс должен быть строковым или массивом строк');
            
            $options['prefix'] = $prefix;
        }    
        
        $cache = new \Phalcon\Cache\Backend\FileExtend(new \Phalcon\Cache\Frontend\Data(), $options);

        $cache->flush();
        
    }

}
