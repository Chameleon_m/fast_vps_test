<?php

namespace FastVps\Common\Plugins\Traits\Models;

trait Change
{

    public function change($field_name)
    {
        $this->{$field_name} = ($this->{$field_name} === 1) ? 0 : 1;

        return $this;
    }

}
