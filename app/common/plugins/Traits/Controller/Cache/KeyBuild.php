<?php

namespace FastVps\Common\Plugins\Traits\Controller\Cache;

trait KeyBuild
{

    /**
     * Вернёт ключ кеша .
     *
     * @return string
     */
    protected function getCacheKey()
    {
        return "_" . $this->dispatcher->getControllerName() .
               "_" . $this->dispatcher->getActionName();
    }

}
