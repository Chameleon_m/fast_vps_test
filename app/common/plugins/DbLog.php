<?php

namespace FastVps\Common\Plugins;

use Phalcon\Events\Event;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\User\Plugin;

class DbLog extends Plugin
{
    private $logger;

    private $profiler;

    public function __construct($file_path) {
        $this->logger = new \Phalcon\Logger\Adapter\File($file_path);
        $this->logger->setFormatter(
            new \Phalcon\Logger\Formatter\Line("%message%")
        );

        $this->profiler = new \Phalcon\Db\Profiler();
    }

//    public function afterConnect()
//    {
//
//    }

    public function beforeQuery($event, $connection)
    {
        $this->profiler->startProfile($connection->getSQLStatement());
    }

    public function afterQuery($event, $connection)
    {
        $this->profiler->stopProfile();
        $this->logger->log("T: " . substr($this->profiler->getTotalElapsedSeconds(),0,7) . " S: " . $connection->getSQLStatement());

    }

    public function getProfiler()
    {
        return $this->profiler;
    }

}