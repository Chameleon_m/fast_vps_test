<?php

define('APPLICATION_PATH', dirname(dirname(__FILE__)) . '/app');
define('PUBLIC_PATH', dirname(dirname(__FILE__)) . '/public');
define('PRIVAT_PATH', dirname(__FILE__));

try {
    /**
     * Используем стандартный для CLI контейнер зависимостей
     */
    $di = new \Phalcon\DI\FactoryDefault\CLI();

    /**
     * Register the global configuration as config 
     */
    $config = include APPLICATION_PATH . '/common/config/config.php';

    $di->set('config', $config);
    /**
     * Создаем консольное приложение
     */
    $application = new \Phalcon\CLI\Console($di);
    $application->registerModules([
        'cli' => [
            'className' => 'FastVps\Cli\Module',
            'path' => APPLICATION_PATH . '/cli/Module.php'
        ],
    ]);

    /**
     * Определяем консольные аргументы
     */
    $params = [];
    switch (count($argv)) {
        case 1:
            $task = 'main';
            $action = 'main';
            break;
        case 2:
            $task = $argv[1];
            $action = 'main';
            break;
        case 3:
            $task = $argv[1];
            $action = $argv[2];
            break;
        default:
            $task = $argv[1];
            $action = $argv[2];
            $params = array_slice($argv, 3);
            break;
    }
    
    $argv = array_merge(
        ['module' => 'cli', 'task' => $task, 'action' => $action],
        $params
    );

    /**
     * Обрабатываем входящие аргументы
     */
    $application->handle($argv);
    
} catch (\Phalcon\Exception $e) {
    echo $e->getMessage();
} catch (\PDOException $e) {
    echo $e->getMessage();
} catch (\Exception $e) {
    echo $e->getMessage();
}