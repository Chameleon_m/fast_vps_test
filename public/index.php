<?php

defined('APPLICATION_PATH') or define('APPLICATION_PATH', dirname(dirname(__FILE__)) . '/app');
defined('PUBLIC_PATH') or define('PUBLIC_PATH', dirname(__FILE__));
defined('APPLICATION_ENV') or define('APPLICATION_ENV', getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production');

try {
    
    /**
     * Include composer autoloader
     */
    require_once __DIR__ . '/../vendor/autoload.php';
    
    /**
     * Read services
     */
    include APPLICATION_PATH . '/common/config/services.php';

    /**
     * Handle the request
     */
    $application = new \Phalcon\Mvc\Application($di);

    /**
     * Register application modules
     */
    $application->registerModules([
        'frontend' => [
            'className' => 'FastVps\Frontend\Module',
            'path' => APPLICATION_PATH . '/frontend/Module.php'
        ],
        'backend' => [
            'className' => 'FastVps\Backend\Module',
            'path' => APPLICATION_PATH . '/backend/Module.php'
        ],
        'api' => [
            'className' => 'FastVps\Api\Module',
            'path' => APPLICATION_PATH . '/api/Module.php'
        ]
    ]);

    echo $application->handle()->getContent();
    
} catch (Phalcon\Exception $e) {
    echo '<pre>' . $e->getMessage() . PHP_EOL;
    echo DEVELOPMENT ? $e->getTraceAsString() : '' ;
} catch (PDOException $e) {
    echo '<pre>' . $e->getMessage() . PHP_EOL;
    echo DEVELOPMENT ? $e->getTraceAsString() : '' ;
} catch (Exception $e) {
    echo '<pre>' . $e->getMessage() . PHP_EOL;
    echo DEVELOPMENT ? $e->getTraceAsString() : '' ;
}
