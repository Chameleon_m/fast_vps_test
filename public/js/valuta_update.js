jQuery(document).ready(function ($) {

    jQuery('#left-menu ul li').click(function () {
        
        var input_child = jQuery(this).children('input');
        // Определяем символынй код валюты
        var char_code = jQuery(this).prop('id');
        
        // эффекты к "меню"
        jQuery(this).toggleClass('check');
        input_child.prop('checked', jQuery(this).hasClass('check'));
        
        // Если уже доставали информацию то скрываем блок
        if (document.getElementById("valuta_" + char_code) != null) {
            jQuery("#valuta_" + char_code).toggle();
            return;
        }
        // ID валюты
        var valuta_id = input_child.val();
        // url конечно лучше генерировать где-то внешне но не сейчас :)
        $.ajax({
            type: "GET",
            url: "/api/valuta/" + valuta_id,
            dataType: "json"
        })
        .done(function (data) {
            $("#valuta_list").prepend("<div class='valuta' id='valuta_" + data.char_code + "'><div class='name'>" + data.name_ru + "</div><div class='value'>" + data.valuta_value + "</div><div class='nominal'>" + data.valuta_nominal + "</div></div>");
        })
        .fail(function () {
            jQuery(this).removeClass('check');
            console.log("Произошла ошибка");
        })
        .always(function () {
            console.log("complete");
        });
        
    });

});