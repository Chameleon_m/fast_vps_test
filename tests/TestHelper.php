<?php

use Phalcon\DI;
use Phalcon\DI\FactoryDefault;

$_SERVER["HTTP_HOST"] = "fast_vps.local";
include '../app/common/config/environment.php';

define('TEST_PATH', __DIR__);
define('ROOT_PATH', dirname(__DIR__));

set_include_path(
        TEST_PATH . PATH_SEPARATOR . get_include_path()
);

require ROOT_PATH . "/vendor/autoload.php";

// Используем автозагрузчик приложений для автозагрузки классов.
// Автозагрузка зависимостей, найденных в composer.
$loader = new \Phalcon\Loader();

$loader->registerDirs([
    TEST_PATH
]);

$loader->registerNamespaces([
    'FastVps\Backend\Controllers' => ROOT_PATH . '/app/backend/controllers/',
    'FastVps\Backend\Models' => ROOT_PATH . '/app/backend/models/',
    'FastVps\Common\Models' => ROOT_PATH . '/app/common/models/',
    'FastVps\Api\Controllers' => ROOT_PATH . '/app/api/controllers/',
    'FastVps\Api\Models' => ROOT_PATH . '/app/api/models/',
    'FastVps\Frontend\Controllers' => ROOT_PATH . '/app/frontend/controllers/',
    'FastVps\Frontend\Models' => ROOT_PATH . '/app/frontend/models/',
]);

$loader->register();

$di = new FactoryDefault();
DI::reset();

// здесь можно добавить любые необходимые сервисы в контейнер зависимостей

include ROOT_PATH . "/app/common/config/services.php";

DI::setDefault($di);
