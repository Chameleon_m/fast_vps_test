<?php

namespace Test;

/**
 * Class UnitTest
 */
class UnitTest extends \UnitTestCase
{

    public function testExtansionTestCase()
    {

        $this->assertTrue(extension_loaded('phalcon'), 'Необходимо установить Phalcon');
        $this->assertTrue(extension_loaded('json'), 'Необходимо установить json');
        $this->assertTrue(extension_loaded('xmlreader'), 'Необходимо установить xmlreader');
    }

    public function testRoutes()
    {
        // Маршруты для проверки
        $testRoutes = [
            '/',
            '/index',
            '/index/index',
            '/api/',
            '/api/valuta',
            '/api/valuta/3',
            '/backend/',
        ];


        $router = $this->getDI()->get('router');

        require_once ROOT_PATH . '/app/frontend/config/routers.php';
        $router->mount(new \FastVps\Frontend\Routers());

        require_once ROOT_PATH . '/app/backend/config/routers.php';
        $router->mount(new \FastVps\Backend\Routers());

        require_once ROOT_PATH . '/app/api/config/routers.php';

        $rest = new \PhalconRest\Mvc\Router\Rest();
        $rest
                ->setNamespace('FastVps\Api\Controllers')
                ->setPrefix('/api')
                ->setIdFilter('[0-9]+')
                ->init()
                ->mountTo($router);

        $router->mount(new \FastVps\Api\Routers());


        $this->assertInstanceOf('Phalcon\Mvc\RouterInterface', $router);

        // Цикл проверки маршрутов
        foreach ($testRoutes as $testRoute) {

            // Обработка маршрута
            $router->handle($testRoute);
            $this->assertTrue($router->wasMatched(), 'Не найден роут ' . $testRoute);
        }
    }

}
